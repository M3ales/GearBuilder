﻿namespace GearBuilder
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.leftSide = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.rightSide = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // leftSide
            // 
            this.leftSide.Location = new System.Drawing.Point(52, 29);
            this.leftSide.Name = "leftSide";
            this.leftSide.Size = new System.Drawing.Size(378, 293);
            this.leftSide.TabIndex = 1;
            this.leftSide.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(52, 328);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(756, 293);
            this.richTextBox2.TabIndex = 2;
            this.richTextBox2.Text = "";
            // 
            // rightSide
            // 
            this.rightSide.Location = new System.Drawing.Point(430, 29);
            this.rightSide.Name = "rightSide";
            this.rightSide.Size = new System.Drawing.Size(378, 293);
            this.rightSide.TabIndex = 3;
            this.rightSide.Text = "";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 692);
            this.ControlBox = false;
            this.Controls.Add(this.rightSide);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.leftSide);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Activated += new System.EventHandler(this.Form2_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form2_FormClosed);
            this.Load += new System.EventHandler(this.Form2_Load);
            this.Shown += new System.EventHandler(this.Form2_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox leftSide;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RichTextBox rightSide;
    }
}