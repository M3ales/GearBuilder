﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GearBuilder
{

    public class Quality
    {
        public string name { get; set; }
        public string darkdisplaycolor { get; set; }
        public string lightdisplaycolor { get; set; }
        public string darkdisplaycolor_hex { get; set; }
        public string lightdisplaycolor_hex { get; set; }
    }

    public class GetTooltip
    {
        public string onmouseout { get; set; }
        public string onmouseover { get; set; }
    }

    public class ItemEnhancementType
    {
        public string name { get; set; }
    }

    public class Enhancement
    {
        public ItemEnhancementType item_enhancement_type { get; set; }
    }

    public class CraftedWithItemInformation
    {
        public int number_crafted_with_item { get; set; }
        public string api_link { get; set; }
    }

    public class RewardedFromMissionsInformation
    {
        public int number_of_missions_rewarded_from { get; set; }
        public string api_link { get; set; }
    }

    public class ModelInformation
    {
        public int id { get; set; }
        public string name { get; set; }
        public string api_link { get; set; }
        public int number_of_items_with_model { get; set; }
    }

    public class ItemSlot
    {
        public string name { get; set; }
    }

    public class GlobalGtnData
    {
        public object current_average { get; set; }
        public object current_high { get; set; }
        public object current_low { get; set; }
    }

    public class searchedItem
    {
        public int id { get; set; }
        public int stack_count { get; set; }
        public int minimum_level { get; set; }
        public int max_durability { get; set; }
        public int value { get; set; }
        public string display_name { get; set; }
        public string description { get; set; }
        public object giftrank { get; set; }
        public object imp_voice_modulation { get; set; }
        public object rep_voice_modulation { get; set; }
        public int armor_rating { get; set; }
        public bool consumed_on_use { get; set; }
        public int unique_limit { get; set; }
        public object required_gender { get; set; }
        public bool social_score_required { get; set; }
        public int social_score_required_rank { get; set; }
        public int required_valor_rank { get; set; }
        public object required_profession_level { get; set; }
        public string api_link { get; set; }
        public string api_icon { get; set; }
        public string disassemble { get; set; }
        public string binding { get; set; }
        public string website_link { get; set; }
        public int item_level { get; set; }
        public object armor_spec { get; set; }
        public object enhancement_type { get; set; }
        public object gift_type { get; set; }
        public Quality quality { get; set; }
        public object use_ability { get; set; }
        public object equip_ability { get; set; }
        public string category { get; set; }
        public string subcategory { get; set; }
        public GetTooltip get_tooltip { get; set; }
        public List<object> combined_stats { get; set; }
        public List<object> base_stats { get; set; }
        public List<Enhancement> enhancements { get; set; }
        public CraftedWithItemInformation crafted_with_item_information { get; set; }
        public RewardedFromMissionsInformation rewarded_from_missions_information { get; set; }
        public List<object> crafted_by { get; set; }
        public ModelInformation model_information { get; set; }
        public List<ItemSlot> item_slots { get; set; }
        public object required_profession { get; set; }
        public GlobalGtnData global_gtn_data { get; set; }
    }

}
