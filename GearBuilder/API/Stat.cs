﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GearBuilder.API
{
    /// <summary>
    /// Used to store both the type and amount of stat on a given Item. It is Immutable, aside from + or += of identical Type
    /// </summary>
    public struct Stat
    {
        public Stat(StatType type, uint amount)
        {
            this.Type = type;
            this.Amount = amount;
        }
        public StatType Type { get; }
        public uint Amount { get; }
        /// <summary>
        /// Sums two Stat objects, throws an InvalidOperationException if the types are not equivalent
        /// </summary>
        /// <param name="a">LHS Stat object</param>
        /// <param name="b">RHS Stat object</param>
        /// <returns>Sum of the two Stats specified</returns>
        public static Stat operator +(Stat a, Stat b)
        {
            if (a.Type != b.Type)
                throw new InvalidOperationException();
            return new Stat(a.Type, a.Amount + b.Amount);
        }
        /// <summary>
        /// Groups the stats by type (all the Mastery gets put together, all crit etc.) and sums their amounts.
        /// </summary>
        /// <param name="summable"></param>
        /// <returns>The sum of all of the same stats under 1 Stat object</returns>
        public static IEnumerable<Stat> SumStats(IStatSummable summable)
        {
            return summable.AllStats
            .GroupBy(s => s.Type)
            .Select(result => new Stat(result.First().Type, (uint)result.Sum(r => r.Amount)))
            .ToList();
        }
    }
    /// <summary>
    /// The stats which an item may posess
    /// </summary>
    public enum StatType
    {
        Null = -1, Mastery, Endurance, Power, Critical, Alacrity, Defence, Absorb, Shield
    }
}
