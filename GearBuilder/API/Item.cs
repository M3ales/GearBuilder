﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GearBuilder.API
{
    /// <summary>
    /// Provides Validation on Item slotting requests, making sure only the correct types of items can be slotted
    /// </summary>
    public class ItemSlot : IStatSummable
    {
        public ItemSlot(ItemType type, Item item = null)
        {
            this.Item = item;
            this.Type = type;
        }
        public Item Item { get; private set; }
        public ItemType Type { get; private set; }
        public void TrySlotItem(Item item)
        {
            if (item.Type != Type)
                throw new ArgumentException(string.Format("Cannot slot item in slot ({0} != {1})", item.Type, Type), "item");
            else
                Item = item;
        }
        public IEnumerable<Stat> TotalStats
        {
            get
            {
                if (Item != null)
                    return Stat.SumStats(Item);
                else
                    return new List<Stat>();
            }
        }

        public IEnumerable<Stat> AllStats
        {
            get
            {
                if (Item != null)
                    return Item.AllStats;
                else
                    return new List<Stat>();
            }
        }
    }
    /// <summary>
    /// Represents any atomic (simplist level) gear item, ie. Enhancement, Augment, Relic etc.
    /// It's worth noting that: Relic/Earpiece/Implant have both GearPiece and an Item. The GearPiece will
    /// handle the augment, not the Item, as an Augment is essentially an Item. The Item refers to the /Base/ stat implementation.
    /// </summary>
    public class Item : IStatSummable
    {
        public Item(string name, ItemType type, int rating)
        {
            this.Name = name;
            this.Rating = rating;
            this.Type = type;
        }
        public string Name { get; }
        public List<Stat> Stats { get; }
        public int Rating { get; }
        public string IconURL { get; }
        /// <summary>
        /// An Item cannot fit into an ItemSlot with a different ItemType
        /// </summary>
        public ItemType Type;
        public IEnumerable<Stat> TotalStats{
            get
            {
                return Stat.SumStats(this);
            }
        }

        public IEnumerable<Stat> AllStats
        {
            get
            {
                return new List<Stat>(Stats);
            }
        }
    }
    /// <summary>
    /// Helps figure out which ItemSlots a given Item may fit into.
    /// </summary>
    public enum ItemType
    {
        Null = -1 , Head, Chest, Gloves, Belt, Legs, Boots, Earpiece, Implant, Bracer, Relic, Mainhand, Offhand, Augment
    }
}
