﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GearBuilder.API.JsonUtils
{
    class JsonRoot
    {
        [JsonProperty("total_items")]
        public int TotalItems;
        [JsonProperty("total_pages")]
        public int TotalPages;
        [JsonProperty("current_page")]
        public int CurrentPage;
        [JsonProperty("sort_order")]
        public string SortOrder;
        [JsonProperty("next_page")]
        public string NextPage;
        [JsonProperty("previous_page")]
        public string PreviousPage;
        [JsonProperty("query")]
        public string Query;
        [JsonProperty("objects")]
        public List<JsonItem> Objects;
    }
}
