﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GearBuilder.API.JsonUtils
{
    class JsonStats
    {
        [JsonProperty("stat_amount")]
        public uint StatAmount;
        [JsonProperty("item_stat_type")]
        public JsonItemStatType StatType;
    }
    class JsonBaseStats
    {
        [JsonProperty("item_stat")]
        public JsonStats ItemStats;
    }
    class JsonItemStatType
    {
        [JsonProperty("name")]
        public string Name;
        [JsonProperty("display_text")]
        public string DisplayText;
    }
}
