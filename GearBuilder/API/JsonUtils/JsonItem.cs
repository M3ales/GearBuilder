﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GearBuilder.API.JsonUtils
{
    class JsonItem
    {
        [JsonProperty("id")]
        public uint ID;
        [JsonProperty("minimum_level")]
        public uint MinimumLevel;
        [JsonProperty("display_name")]
        public string DisplayName;
        [JsonProperty("armor_rating")]
        public int ArmourRating;
        [JsonProperty("api_link")]
        public string APIURL;
        [JsonProperty("api_icon")]
        public string APIIconURL;
        [JsonProperty("item_level")]
        public uint ItemLevel;
        [JsonProperty("value")]
        public uint Value;
        [JsonProperty("quality")]
        public JsonItemQuality Quality;
        [JsonProperty("category")]
        public string Category;
        [JsonProperty("subcategory")]
        public string Subcategory;
        [JsonProperty("combined_stats")]
        public List<JsonStats> CombinedStats;
        [JsonProperty("base_stats")]
        public List<JsonBaseStats> BaseStats;
        [JsonProperty("binding")]
        public string Binding;
        [JsonProperty("crafted_by")]
        public List<JsonCraftedItem> CraftedBy;
        public class JsonCraftedItem
        {
            [JsonProperty("crafted_item")]
            public JsonItem CraftedItem;
        }
    }
    class JsonItemQuality
    {
        [JsonProperty("name")]
        public string Name;
        [JsonProperty("darkdisplaycolor")]
        public string DarkDisplayColour;
        [JsonProperty("lightdisplaycolor")]
        public string LightDisplayColour;
        [JsonProperty("darkdisplaycolor_hex")]
        public string DarkDisplayColourHex;
        [JsonProperty("lightdisplaycolor_hex")]
        public string LightDisplayColourHex;
    }
}
