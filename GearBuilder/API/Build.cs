﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GearBuilder.API
{
    public class Build : IStatSummable
    {
        public Build()
        {
            GearPieces = new List<GearPiece>();
            Head = new GearPiece("Head");
            GearPieces.Add(Head);
            Chest = new GearPiece("Chest");
            GearPieces.Add(Chest);
            Gloves = new GearPiece("Gloves");
            GearPieces.Add(Gloves);
            Belt = new GearPiece("Belt", 2);
            GearPieces.Add(Belt);
            Legs = new GearPiece("Legs");
            GearPieces.Add(Legs);
            Boots = new GearPiece("Boots");
            GearPieces.Add(Boots);
            Earpiece = new GearPiece("Earpiece",1);
            GearPieces.Add(Earpiece);
            Implants = new GearPiece[2];
            Implants[0] = new GearPiece("Implant",1);
            Implants[1] = new GearPiece("Implant", 1);
            GearPieces.AddRange(Implants);
            Bracer = new GearPiece("Bracer", 2);
            GearPieces.Add(Bracer);
            Relics = new GearPiece[2];
            Relics[0] = new GearPiece("Relic", 1);
            Relics[1] = new GearPiece("Relic", 1);
            GearPieces.AddRange(Relics);
            Mainhand = new GearPiece("Mainhand",4);
            GearPieces.Add(Mainhand);
            Offhand = new GearPiece("Offhand",4);
            GearPieces.Add(Offhand);
        }
        public List<GearPiece> GearPieces;
        public GearPiece Head { get; private set; }
        public GearPiece Chest { get; private set; }
        public GearPiece Gloves { get; private set; }
        public GearPiece Belt { get; private set; }
        public GearPiece Legs { get; private set; }
        public GearPiece Boots { get; private set; }
        public GearPiece Earpiece { get; private set; }
        public GearPiece[] Implants { get; private set; }
        public GearPiece Bracer { get; private set; }
        public GearPiece[] Relics { get; private set; }
        public GearPiece Mainhand { get; private set; }
        public GearPiece Offhand { get; private set; }

        public IEnumerable<Stat> TotalStats
        {
            get
            {
                return Stat.SumStats(this);
            }
        }
        public IEnumerable<Stat> AllStats
        {
            get
            {
                return GearPieces.SelectMany(gear => gear.AllStats);
            }
        }
    }
}
 