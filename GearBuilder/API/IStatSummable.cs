﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GearBuilder.API
{
    public interface IStatSummable
    {
        /// <summary>
        /// Distinct sum of the stats
        /// </summary>
        IEnumerable<Stat> TotalStats { get; }
        /// <summary>
        /// Combined list of all stats objects on this, and all the stats on it's subobjects
        /// </summary>
        IEnumerable<Stat> AllStats { get; }
    }
}
