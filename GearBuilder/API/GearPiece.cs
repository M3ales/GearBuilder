﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GearBuilder.API
{
    public class GearPiece : IStatSummable
    {
        public GearPiece(string name, uint itemSlots = 3)
        {
            this.Name = name;
            Items = new ItemSlot[itemSlots];
        }

        public string Name;
        /// <summary>
        /// All the Items Slotted in this Item. Is a fixed length array to prevent additional items from being added.
        /// </summary>
        public ItemSlot[] Items;
        /// <summary>
        /// The gear item's Augment, null if the slot is empty.
        /// </summary>
        public ItemSlot Augment;
        public IEnumerable<Stat> TotalStats
        {
            get
            {
                return Stat.SumStats(this);
            }        
        }
        /// <summary>
        /// Attempts to place the item in a slot on this GearPiece, throws ArugmentException if no valid slots are present
        /// </summary>
        /// <param name="i"></param>
        public void EquipItem(Item i)
        {
            //If it's an augment, slot it in augment slot
            if(i.Type == ItemType.Augment)
                Augment.TrySlotItem(i);
            //else check if this GearPiece supports this type
            if (Items.Where(item => item.Type == i.Type).Count() == 0)
                throw new ArgumentException(string.Format("Cannot slot Type {0} in any slots.", i.Type), "i");
            //if it does support the type, slot it. 
            //(There should never be more than one instance of each StatType on a GearPiece)
            Items.First(item => item.Type == i.Type).TrySlotItem(i);
        }
        public IEnumerable<Stat> AllStats => Items.SelectMany(i => i.AllStats);
    }
}
