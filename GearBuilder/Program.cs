﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using GearBuilder.API;
using GearBuilder.API.JsonUtils;
using System.IO;

namespace GearBuilder
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            MiraTest();
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
        }
        static void MiraTest()
        {
            //Should load EVERY .json file in the specified root folder
            //replace this path with something relevant to your current setup (Since downloads don't happen 
            //they should point to an already downloaded copy)

            //TODO: Write this to accept stuff from the download logic (perhaps separate download logic into API first)
            string pathToItemFolder = @"C:\Users\Matteo\Downloads\GMS";
            Parser p;
            List<JsonItem> i = new List<JsonItem>();
            foreach (string f in Directory.EnumerateFiles(pathToItemFolder + @"\Items\"))
            {
                if (f.EndsWith(".json"))
                {
                    p = new Parser(new StreamReader(new FileStream(f, FileMode.Open)));
                    //Filter out all the crafted elements
                    List<JsonItem> temp = p.root.Objects
                        .Where(x => x.CraftedBy.Count > 0)
                        .Where(x => x.CraftedBy.First().CraftedItem != null)
                        .ToList();

                    //Add (crafted) to elements that are 'crafted'
                    temp.Where(x=> x.Binding != "OnPickup")
                        .ToList()
                        .ForEach(x => x.DisplayName += " (Crafted)");
                    temp.Where(x => x.Binding == "OnPickup")
                        .ToList()
                        .ForEach(x => x.DisplayName += " (Crafted BoP)");
                    //Combine the crafted items with the rest of the normal items via Union, then throw away duplicates with Distinct
                    i.AddRange(temp
                        .Union(p.root.Objects.Where(x => x.Value > 0))
                        .Distinct()//rip duplicates
                        .ToList());
                }
            }
            i = i.OrderBy(item => item.ArmourRating).ToList();
            i.ForEach(x => Console.WriteLine(x.ID + " : " + x.ArmourRating + " : " + x.Binding + " : " + x.Quality.Name + " : "+ x.DisplayName));
            Console.WriteLine("Done");
        }
    }
}
