﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GearBuilder
{
    public class FullGear
    {
        public List<GearPiece> fullGear = new List<GearPiece>();

        public String displayFullGearInfo()
        {
            String s = "";
            foreach (var v in fullGear)
            {
                s = s + "--------------------------------------\n";
                s = s + v.GearPieceInfo();
            }
            s = s + "--------------------------------------\n";
            return s;
        }
        public String displayFullGearInfo(int side)
        {
            String s = "";
            int i = 0;
            foreach (var v in fullGear)
            {
                switch (side)
                {
                    case 1:
                        if (i == (fullGear.Count / 2)) return s;
                        break;
                    case 2:
                        i++;
                        if (i <= fullGear.Count / 2) continue;
                        break;
                }
               
                s = s + "--------------------------------------\n";
                s = s + v.GearPieceInfo();
                i++; 
            }
            s = s + "--------------------------------------\n";
            return s;
        }
    }
}
