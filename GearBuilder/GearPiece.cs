﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GearBuilder
{
    public class GearPiece
    {
        public StatBlock armoring = new StatBlock();
        public StatBlock mod = new StatBlock();
        public StatBlock enhancement = new StatBlock();
        public StatBlock augment = new StatBlock();
        public StatBlock misc = new StatBlock();
        public Form1.GEAR type;


    
        public string GearPieceInfo()
        {
            String sb = null;

            int Mastery;
            int Endurance;
            int Critical;
            int Power;
            int Alacrity;
            int Accuracy;
            int Defence;
            int Absorb;
            int Shield;
           
            sb = sb + ((type == null) ? "" : type.ToString()) + "\n";
            sb = sb + "--------------------------------------\n";
            Mastery = ((armoring == null) ? 0 : armoring.Mastery) + ((mod == null) ? 0 : mod.Mastery) + ((enhancement == null) ? 0 : enhancement.Mastery) + ((augment == null) ? 0 : augment.Mastery) + ((misc == null) ? 0 : misc.Mastery);
            Endurance = ((armoring == null) ? 0 : armoring.Endurance) + ((mod == null) ? 0 : mod.Endurance) + ((enhancement == null) ? 0 : enhancement.Endurance) + ((augment == null) ? 0 : augment.Endurance) + ((misc == null) ? 0 : misc.Endurance);
            Critical = ((armoring == null) ? 0 : armoring.Critical) + ((mod == null) ? 0 : mod.Critical) + ((enhancement == null) ? 0 : enhancement.Critical) + ((augment == null) ? 0 : augment.Critical) + ((misc == null) ? 0 : misc.Critical);
            Power = ((armoring == null) ? 0 : armoring.Power) + ((mod == null) ? 0 : mod.Power) + ((enhancement == null) ? 0 : enhancement.Power) + ((augment == null) ? 0 : augment.Power) + ((misc == null) ? 0 : misc.Power);
            Alacrity = ((armoring == null) ? 0 : armoring.Alacrity) + ((mod == null) ? 0 : mod.Alacrity) + ((enhancement == null) ? 0 : enhancement.Alacrity) + ((augment == null) ? 0 : augment.Alacrity) + ((misc == null) ? 0 : misc.Alacrity);
            Accuracy = ((armoring == null) ? 0 : armoring.Accuracy) + ((mod == null) ? 0 : mod.Accuracy) + ((enhancement == null) ? 0 : enhancement.Accuracy) + ((augment == null) ? 0 : augment.Accuracy) + ((misc == null) ? 0 : misc.Accuracy);
            Defence = ((armoring == null) ? 0 : armoring.Defence) + ((mod == null) ? 0 : mod.Defence) + ((enhancement == null) ? 0 : enhancement.Defence) + ((augment == null) ? 0 : augment.Defence) + ((misc == null) ? 0 : misc.Defence);
            Absorb = ((armoring == null) ? 0 : armoring.Absorb) + ((mod == null) ? 0 : mod.Absorb) + ((enhancement == null) ? 0 : enhancement.Absorb) + ((augment == null) ? 0 : augment.Absorb) + ((misc == null) ? 0 : misc.Absorb);
            Shield = ((armoring == null) ? 0 : armoring.Shield) + ((mod == null) ? 0 : mod.Shield) + ((enhancement == null) ? 0 : enhancement.Shield) + ((augment == null) ? 0 : augment.Shield) + ((misc == null) ? 0 : misc.Shield);
            sb = sb + ((Mastery != 0) ? "Mastery:\t\t" + Mastery + "\n" : "");
            sb = sb + ((Endurance != 0) ? "Endurance:\t" + Endurance + "\n" : "");
            sb = sb + ((Critical != 0) ? "Critical:\t\t" + Critical + "\n" : "");
            sb = sb + ((Power != 0) ? "Power:\t\t" + Power + "\n" : "");
            sb = sb + ((Alacrity != 0) ? "Alacrity:\t\t" + Alacrity + "\n" : "");
            sb = sb + ((Accuracy != 0) ? "Accuracy:\t" + Accuracy + "\n" : "");
            sb = sb + ((Defence != 0) ? "Defence:\t\t" + Defence + "\n" : "");
            sb = sb + ((Absorb != 0) ? "Absorb:\t\t" + Absorb + "\n" : "");
            sb = sb + ((Shield != 0) ? "Shield:\t\t" + Shield + "\n" : "");
           // sb = sb + "\n-----------------------------------------\n";
            return sb;
        }
    }
}
