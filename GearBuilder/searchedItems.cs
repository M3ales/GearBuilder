﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GearBuilder
{

    
    public class ItemStatType
    {
        public string name { get; set; }
        public string display_text { get; set; }
    }

    public class CombinedStat
    {
        public int stat_amount { get; set; }
        public ItemStatType item_stat_type { get; set; }
    }
    public class ItemStatType2
    {
        public string name { get; set; }
        public string display_text { get; set; }
    }
    public class ItemStat
    {
        public int stat_amount { get; set; }
        public ItemStatType2 item_stat_type { get; set; }
    }
    public class BaseStat
    {
        public ItemStat item_stat { get; set; }
    }
    public class Object
    {
        public int id { get; set; }
        public int stack_count { get; set; }
        public int minimum_level { get; set; }
        public int max_durability { get; set; }
        public int value { get; set; }
        public string display_name { get; set; }
        public string description { get; set; }
        public object giftrank { get; set; }
        public object imp_voice_modulation { get; set; }
        public object rep_voice_modulation { get; set; }
        public int armor_rating { get; set; }
        public bool consumed_on_use { get; set; }
        public int unique_limit { get; set; }
        public object required_gender { get; set; }
        public bool social_score_required { get; set; }
        public int social_score_required_rank { get; set; }
        public int required_valor_rank { get; set; }
        public object required_profession_level { get; set; }
        public string api_link { get; set; }
        public string api_icon { get; set; }
        public string disassemble { get; set; }
        public string binding { get; set; }
        public string website_link { get; set; }
        public int item_level { get; set; }
        public object armor_spec { get; set; }
        public object enhancement_type { get; set; }
        public object gift_type { get; set; }
        public Quality quality { get; set; }
        public object use_ability { get; set; }
        public object equip_ability { get; set; }
        public string category { get; set; }
        public string subcategory { get; set; }
        public GetTooltip get_tooltip { get; set; }
        public List<CombinedStat> combined_stats { get; set; }
        public List<BaseStat> base_stats { get; set; }
        public List<object> enhancements { get; set; }
        public CraftedWithItemInformation crafted_with_item_information { get; set; }
        public RewardedFromMissionsInformation rewarded_from_missions_information { get; set; }
        public List<object> crafted_by { get; set; }
        public object model_information { get; set; }
        public List<ItemSlot> item_slots { get; set; }
        public object required_profession { get; set; }
        public GlobalGtnData global_gtn_data { get; set; }
    }

    public class searchedItems
    {
        public int total_items { get; set; }
        public int total_pages { get; set; }
        public int current_page { get; set; }
        public object sort_order { get; set; }
        public int items_per_pags { get; set; }
        public string next_page { get; set; }
        public object previous_page { get; set; }
        public string query { get; set; }
        public List<Object> objects { get; set; }
    }
}
