﻿namespace GearBuilder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.downloadThis = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.augmentHead = new System.Windows.Forms.ComboBox();
            this.enhancementHead = new System.Windows.Forms.ComboBox();
            this.modHead = new System.Windows.Forms.ComboBox();
            this.armoringHead = new System.Windows.Forms.ComboBox();
            this.fourPanel = new System.Windows.Forms.Panel();
            this.Augment = new System.Windows.Forms.ComboBox();
            this.Armoring = new System.Windows.Forms.ComboBox();
            this.Mod = new System.Windows.Forms.ComboBox();
            this.Enhancement = new System.Windows.Forms.ComboBox();
            this.Relic = new System.Windows.Forms.ComboBox();
            this.Earpiece = new System.Windows.Forms.ComboBox();
            this.Implant = new System.Windows.Forms.ComboBox();
            this.ArmoringInfo = new System.Windows.Forms.Label();
            this.ModInfo = new System.Windows.Forms.Label();
            this.AugmentInfo = new System.Windows.Forms.Label();
            this.EnhancementInfo = new System.Windows.Forms.Label();
            this.EarpieceInfo = new System.Windows.Forms.Label();
            this.RelicInfo = new System.Windows.Forms.Label();
            this.ImplantInfo = new System.Windows.Forms.Label();
            this.charPanel = new System.Windows.Forms.Panel();
            this.Crystal = new System.Windows.Forms.ComboBox();
            this.CrystalInfo = new System.Windows.Forms.Label();
            this.searchPanel = new System.Windows.Forms.Panel();
            this.infoPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.charLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.infoLabel = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.fourPanel.SuspendLayout();
            this.searchPanel.SuspendLayout();
            this.infoPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(-1, 31);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(233, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "Basic Download";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // downloadThis
            // 
            this.downloadThis.Location = new System.Drawing.Point(-1, -1);
            this.downloadThis.Margin = new System.Windows.Forms.Padding(4);
            this.downloadThis.Name = "downloadThis";
            this.downloadThis.Size = new System.Drawing.Size(395, 22);
            this.downloadThis.TabIndex = 1;
            this.downloadThis.Text = "query=advanced&minimum_level_gte=65&perpage=50";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(-1, 133);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(395, 259);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(-1, 102);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(395, 24);
            this.comboBox1.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(-1, 66);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(233, 28);
            this.button2.TabIndex = 5;
            this.button2.Text = "Additional Download";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(240, 32);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(153, 22);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = "Augment";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(240, 66);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(155, 28);
            this.button3.TabIndex = 7;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // augmentHead
            // 
            this.augmentHead.FormattingEnabled = true;
            this.augmentHead.Location = new System.Drawing.Point(89, 25);
            this.augmentHead.Margin = new System.Windows.Forms.Padding(4);
            this.augmentHead.Name = "augmentHead";
            this.augmentHead.Size = new System.Drawing.Size(88, 24);
            this.augmentHead.TabIndex = 40;
            // 
            // enhancementHead
            // 
            this.enhancementHead.FormattingEnabled = true;
            this.enhancementHead.Location = new System.Drawing.Point(0, 25);
            this.enhancementHead.Margin = new System.Windows.Forms.Padding(4);
            this.enhancementHead.Name = "enhancementHead";
            this.enhancementHead.Size = new System.Drawing.Size(88, 24);
            this.enhancementHead.TabIndex = 39;
            // 
            // modHead
            // 
            this.modHead.FormattingEnabled = true;
            this.modHead.Location = new System.Drawing.Point(89, 0);
            this.modHead.Margin = new System.Windows.Forms.Padding(4);
            this.modHead.Name = "modHead";
            this.modHead.Size = new System.Drawing.Size(88, 24);
            this.modHead.TabIndex = 38;
            // 
            // armoringHead
            // 
            this.armoringHead.FormattingEnabled = true;
            this.armoringHead.Location = new System.Drawing.Point(0, 0);
            this.armoringHead.Margin = new System.Windows.Forms.Padding(4);
            this.armoringHead.Name = "armoringHead";
            this.armoringHead.Size = new System.Drawing.Size(88, 24);
            this.armoringHead.TabIndex = 37;
            // 
            // fourPanel
            // 
            this.fourPanel.Controls.Add(this.armoringHead);
            this.fourPanel.Controls.Add(this.augmentHead);
            this.fourPanel.Controls.Add(this.modHead);
            this.fourPanel.Controls.Add(this.enhancementHead);
            this.fourPanel.Location = new System.Drawing.Point(-1, 288);
            this.fourPanel.Margin = new System.Windows.Forms.Padding(4);
            this.fourPanel.Name = "fourPanel";
            this.fourPanel.Size = new System.Drawing.Size(181, 103);
            this.fourPanel.TabIndex = 41;
            this.fourPanel.Visible = false;
            // 
            // Augment
            // 
            this.Augment.FormattingEnabled = true;
            this.Augment.Location = new System.Drawing.Point(-1, 101);
            this.Augment.Margin = new System.Windows.Forms.Padding(4);
            this.Augment.Name = "Augment";
            this.Augment.Size = new System.Drawing.Size(332, 24);
            this.Augment.TabIndex = 42;
            this.Augment.Text = "Augment";
            // 
            // Armoring
            // 
            this.Armoring.FormattingEnabled = true;
            this.Armoring.Location = new System.Drawing.Point(-1, 1);
            this.Armoring.Margin = new System.Windows.Forms.Padding(4);
            this.Armoring.Name = "Armoring";
            this.Armoring.Size = new System.Drawing.Size(332, 24);
            this.Armoring.TabIndex = 43;
            this.Armoring.Text = "Armoring";
            // 
            // Mod
            // 
            this.Mod.FormattingEnabled = true;
            this.Mod.Location = new System.Drawing.Point(-1, 34);
            this.Mod.Margin = new System.Windows.Forms.Padding(4);
            this.Mod.Name = "Mod";
            this.Mod.Size = new System.Drawing.Size(332, 24);
            this.Mod.TabIndex = 44;
            this.Mod.Text = "Mod";
            // 
            // Enhancement
            // 
            this.Enhancement.FormattingEnabled = true;
            this.Enhancement.Location = new System.Drawing.Point(-1, 68);
            this.Enhancement.Margin = new System.Windows.Forms.Padding(4);
            this.Enhancement.Name = "Enhancement";
            this.Enhancement.Size = new System.Drawing.Size(332, 24);
            this.Enhancement.TabIndex = 45;
            this.Enhancement.Text = "Enhancement";
            // 
            // Relic
            // 
            this.Relic.FormattingEnabled = true;
            this.Relic.Location = new System.Drawing.Point(-1, 134);
            this.Relic.Margin = new System.Windows.Forms.Padding(4);
            this.Relic.Name = "Relic";
            this.Relic.Size = new System.Drawing.Size(332, 24);
            this.Relic.TabIndex = 46;
            this.Relic.Text = "Relic";
            // 
            // Earpiece
            // 
            this.Earpiece.FormattingEnabled = true;
            this.Earpiece.Location = new System.Drawing.Point(-1, 167);
            this.Earpiece.Margin = new System.Windows.Forms.Padding(4);
            this.Earpiece.Name = "Earpiece";
            this.Earpiece.Size = new System.Drawing.Size(332, 24);
            this.Earpiece.TabIndex = 47;
            this.Earpiece.Text = "Earpiece";
            // 
            // Implant
            // 
            this.Implant.FormattingEnabled = true;
            this.Implant.Location = new System.Drawing.Point(-1, 201);
            this.Implant.Margin = new System.Windows.Forms.Padding(4);
            this.Implant.Name = "Implant";
            this.Implant.Size = new System.Drawing.Size(332, 24);
            this.Implant.TabIndex = 48;
            this.Implant.Text = "Implant";
            // 
            // ArmoringInfo
            // 
            this.ArmoringInfo.AutoSize = true;
            this.ArmoringInfo.Location = new System.Drawing.Point(340, 1);
            this.ArmoringInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ArmoringInfo.Name = "ArmoringInfo";
            this.ArmoringInfo.Size = new System.Drawing.Size(92, 17);
            this.ArmoringInfo.TabIndex = 49;
            this.ArmoringInfo.Text = "Armoring Info";
            // 
            // ModInfo
            // 
            this.ModInfo.AutoSize = true;
            this.ModInfo.Location = new System.Drawing.Point(340, 36);
            this.ModInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ModInfo.Name = "ModInfo";
            this.ModInfo.Size = new System.Drawing.Size(62, 17);
            this.ModInfo.TabIndex = 50;
            this.ModInfo.Text = "Mod Info";
            // 
            // AugmentInfo
            // 
            this.AugmentInfo.AutoSize = true;
            this.AugmentInfo.Location = new System.Drawing.Point(340, 102);
            this.AugmentInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.AugmentInfo.Name = "AugmentInfo";
            this.AugmentInfo.Size = new System.Drawing.Size(87, 17);
            this.AugmentInfo.TabIndex = 52;
            this.AugmentInfo.Text = "AugmentInfo";
            // 
            // EnhancementInfo
            // 
            this.EnhancementInfo.AutoSize = true;
            this.EnhancementInfo.Location = new System.Drawing.Point(340, 68);
            this.EnhancementInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.EnhancementInfo.Name = "EnhancementInfo";
            this.EnhancementInfo.Size = new System.Drawing.Size(122, 17);
            this.EnhancementInfo.TabIndex = 51;
            this.EnhancementInfo.Text = "Enhancement Info";
            // 
            // EarpieceInfo
            // 
            this.EarpieceInfo.AutoSize = true;
            this.EarpieceInfo.Location = new System.Drawing.Point(340, 170);
            this.EarpieceInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.EarpieceInfo.Name = "EarpieceInfo";
            this.EarpieceInfo.Size = new System.Drawing.Size(91, 17);
            this.EarpieceInfo.TabIndex = 54;
            this.EarpieceInfo.Text = "Earpiece Info";
            // 
            // RelicInfo
            // 
            this.RelicInfo.AutoSize = true;
            this.RelicInfo.Location = new System.Drawing.Point(340, 135);
            this.RelicInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.RelicInfo.Name = "RelicInfo";
            this.RelicInfo.Size = new System.Drawing.Size(66, 17);
            this.RelicInfo.TabIndex = 53;
            this.RelicInfo.Text = "Relic Info";
            // 
            // ImplantInfo
            // 
            this.ImplantInfo.AutoSize = true;
            this.ImplantInfo.Location = new System.Drawing.Point(340, 201);
            this.ImplantInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ImplantInfo.Name = "ImplantInfo";
            this.ImplantInfo.Size = new System.Drawing.Size(80, 17);
            this.ImplantInfo.TabIndex = 55;
            this.ImplantInfo.Text = "Implant Info";
            // 
            // charPanel
            // 
            this.charPanel.BackColor = System.Drawing.Color.Transparent;
            this.charPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.charPanel.Location = new System.Drawing.Point(420, 33);
            this.charPanel.Margin = new System.Windows.Forms.Padding(4);
            this.charPanel.Name = "charPanel";
            this.charPanel.Size = new System.Drawing.Size(722, 393);
            this.charPanel.TabIndex = 41;
            this.charPanel.Visible = false;
            // 
            // Crystal
            // 
            this.Crystal.FormattingEnabled = true;
            this.Crystal.Location = new System.Drawing.Point(-1, 234);
            this.Crystal.Margin = new System.Windows.Forms.Padding(4);
            this.Crystal.Name = "Crystal";
            this.Crystal.Size = new System.Drawing.Size(332, 24);
            this.Crystal.TabIndex = 56;
            this.Crystal.Text = "Crystal";
            // 
            // CrystalInfo
            // 
            this.CrystalInfo.AutoSize = true;
            this.CrystalInfo.Location = new System.Drawing.Point(340, 238);
            this.CrystalInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.CrystalInfo.Name = "CrystalInfo";
            this.CrystalInfo.Size = new System.Drawing.Size(78, 17);
            this.CrystalInfo.TabIndex = 57;
            this.CrystalInfo.Text = "Crystal Info";
            // 
            // searchPanel
            // 
            this.searchPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchPanel.Controls.Add(this.downloadThis);
            this.searchPanel.Controls.Add(this.richTextBox1);
            this.searchPanel.Controls.Add(this.button1);
            this.searchPanel.Controls.Add(this.comboBox1);
            this.searchPanel.Controls.Add(this.button2);
            this.searchPanel.Controls.Add(this.textBox1);
            this.searchPanel.Controls.Add(this.button3);
            this.searchPanel.Location = new System.Drawing.Point(16, 32);
            this.searchPanel.Margin = new System.Windows.Forms.Padding(4);
            this.searchPanel.Name = "searchPanel";
            this.searchPanel.Size = new System.Drawing.Size(395, 393);
            this.searchPanel.TabIndex = 42;
            this.searchPanel.Visible = false;
            // 
            // infoPanel
            // 
            this.infoPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.infoPanel.Controls.Add(this.Augment);
            this.infoPanel.Controls.Add(this.CrystalInfo);
            this.infoPanel.Controls.Add(this.Armoring);
            this.infoPanel.Controls.Add(this.Crystal);
            this.infoPanel.Controls.Add(this.Mod);
            this.infoPanel.Controls.Add(this.fourPanel);
            this.infoPanel.Controls.Add(this.Enhancement);
            this.infoPanel.Controls.Add(this.ImplantInfo);
            this.infoPanel.Controls.Add(this.Relic);
            this.infoPanel.Controls.Add(this.EarpieceInfo);
            this.infoPanel.Controls.Add(this.Earpiece);
            this.infoPanel.Controls.Add(this.RelicInfo);
            this.infoPanel.Controls.Add(this.Implant);
            this.infoPanel.Controls.Add(this.AugmentInfo);
            this.infoPanel.Controls.Add(this.ArmoringInfo);
            this.infoPanel.Controls.Add(this.EnhancementInfo);
            this.infoPanel.Controls.Add(this.ModInfo);
            this.infoPanel.Location = new System.Drawing.Point(1151, 33);
            this.infoPanel.Margin = new System.Windows.Forms.Padding(4);
            this.infoPanel.Name = "infoPanel";
            this.infoPanel.Size = new System.Drawing.Size(670, 393);
            this.infoPanel.TabIndex = 42;
            this.infoPanel.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(343, 17);
            this.label1.TabIndex = 58;
            this.label1.Text = "SEARCH OPTIONS - DON\'T TOUCH, NOT FINISHED";
            this.label1.Visible = false;
            // 
            // charLabel
            // 
            this.charLabel.AutoSize = true;
            this.charLabel.Location = new System.Drawing.Point(691, 11);
            this.charLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.charLabel.Name = "charLabel";
            this.charLabel.Size = new System.Drawing.Size(100, 17);
            this.charLabel.TabIndex = 59;
            this.charLabel.Text = "GEAR PICKER";
            this.charLabel.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1379, 11);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 17);
            this.label3.TabIndex = 60;
            this.label3.Text = "GEAR INFO";
            this.label3.Visible = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(149, 797);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 87);
            this.button4.TabIndex = 61;
            this.button4.Text = "Display Stats";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Location = new System.Drawing.Point(309, 766);
            this.infoLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(16, 17);
            this.infoLabel.TabIndex = 58;
            this.infoLabel.Text = "0";
            this.infoLabel.Visible = false;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(282, 797);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(100, 87);
            this.button5.TabIndex = 62;
            this.button5.Text = "Exit";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1824, 999);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.charLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.infoPanel);
            this.Controls.Add(this.searchPanel);
            this.Controls.Add(this.charPanel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.LocationChanged += new System.EventHandler(this.Form1_LocationChanged);
            this.fourPanel.ResumeLayout(false);
            this.searchPanel.ResumeLayout(false);
            this.searchPanel.PerformLayout();
            this.infoPanel.ResumeLayout(false);
            this.infoPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox downloadThis;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox augmentHead;
        private System.Windows.Forms.ComboBox enhancementHead;
        private System.Windows.Forms.ComboBox modHead;
        private System.Windows.Forms.ComboBox armoringHead;
        private System.Windows.Forms.Panel fourPanel;
        private System.Windows.Forms.ComboBox Augment;
        private System.Windows.Forms.ComboBox Armoring;
        private System.Windows.Forms.ComboBox Mod;
        private System.Windows.Forms.ComboBox Enhancement;
        private System.Windows.Forms.ComboBox Relic;
        private System.Windows.Forms.ComboBox Earpiece;
        private System.Windows.Forms.ComboBox Implant;
        private System.Windows.Forms.Label ArmoringInfo;
        private System.Windows.Forms.Label ModInfo;
        private System.Windows.Forms.Label AugmentInfo;
        private System.Windows.Forms.Label EnhancementInfo;
        private System.Windows.Forms.Label EarpieceInfo;
        private System.Windows.Forms.Label RelicInfo;
        private System.Windows.Forms.Label ImplantInfo;
        private System.Windows.Forms.Panel charPanel;
        private System.Windows.Forms.ComboBox Crystal;
        private System.Windows.Forms.Label CrystalInfo;
        private System.Windows.Forms.Panel searchPanel;
        private System.Windows.Forms.Panel infoPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label charLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

