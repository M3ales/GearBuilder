﻿

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using GearBuilder.API.JsonUtils;

namespace GearBuilder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static T loadJson<T>(string path)
        {
            try
            {
                using (StreamReader reader = File.OpenText(path))
                {
                    var classJson = new JavaScriptSerializer().Deserialize<T>(reader.ReadToEnd());
                    Debug.WriteLine("Loading JSON for: " + classJson.ToString());
                    return classJson;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                Debug.WriteLine("Could not open File");
            }
            return default(T);
        }
        public static int loadTimer = 0;
        public static searchedItems si;
        private async void button1_Click(object sender, EventArgs e)
        {
            //http://swtordata.com:80/api/v2_2/items?perpage=50&query=advanced%20versatile%20augment&sortorder=display_name_asc&moredetailed=true
            //http://cdn.swtordata.com/icons/kotet_augment_blue.png api_icon
            //Parameter    =   Value                        Description                                    Parameter Type      Data Type
            //page         =                                number.Defaults to 1                           query               integer
            //perpage      =   50                           Items Per Page.Max 50                          query               integer
            //query        =   advanced versatile augment   Search Items                                   query               string
            //sortorder    =   display_name asc             Filter.Will Default too: 'display_name asc'    query               string
            //item_bind    =                                Filter.No value = any binding                  query               string
            //item_quality =                                Filter.                                        query               string
            //moredetailed =  true                          true = 20 per page                             query               string
            //item_slots   =                                head, augment, bla bla                         query               string
            //minimum_level_gte                                                                            query               integer
            //minimum_level_lt                                                                             query               integer


            //after experimentation with the API, the conclusion is that downloading the data must be done beforehand to ensure offline-mode
            //alternatively, real-life search can be implemented to help ease-in item finding.
            //for each slot {head, chest} -> for each modification {armoring, mod, enh} -> open appropriate menu {adv versatile selection}
            //build gear
            //export/save


            //    client.DefaultRequestHeaders.Add("X-Api-Key", "gvqprHwwEIOX8rPBQiLA52cauxkyBOrJIixgWePH");
            try
            {
                using (var client = new HttpClient())
                {

                    Debug.WriteLine("Downloading: " + downloadThis.Text);
                    client.DefaultRequestHeaders.Add("X-Api-Key", "gvqprHwwEIOX8rPBQiLA52cauxkyBOrJIixgWePH");
                    Uri URI = new Uri("http://swtordata.com:80/api/v2_2/items?" + downloadThis.Text);
                    //     var itemName = downloadThis.Text.Split('/');
                    using (var response = await client.GetAsync(URI))
                    using (var stream = new FileStream(Application.StartupPath + "\\search_test.json", FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        await response.Content.CopyToAsync(stream);
                    }

                    //Debug.WriteLine("Download completed");
                    si = loadJson<searchedItems>(Application.StartupPath + "\\search_test.json");
                    foreach (var s in si.objects)
                    {
                        comboBox1.Items.Add(s.display_name + "    (Rating: " + s.armor_rating + ")");
                    }

                    Debug.WriteLine("Done");
                    Debug.WriteLine("Items downloaded so far: " + comboBox1.Items.Count + ", current page: " + si.current_page + ", total pages: " + si.total_pages + ", total items: " + si.total_items);
                    using (StreamReader sr = File.OpenText(Application.StartupPath + "\\search_test.json"))
                    {
                        string s = sr.ReadToEnd();
                        richTextBox1.Text = richTextBox1.Text + s;
                    }

                    //URI = new Uri(si.api_icon);
                    ////     var itemName = downloadThis.Text.Split('/');
                    //using (var response = await client.GetAsync(URI))
                    //using (var stream = new FileStream(Application.StartupPath + "\\saber.sithlow02.a01_v01.png", FileMode.Create, FileAccess.Write, FileShare.None))
                    //{
                    //    await response.Content.CopyToAsync(stream);
                    //}

                }


            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Debug.WriteLine("Could not download data, skipping");
            }

            // Debug.WriteLine(si.display_name.ToString());

        }
        public static int page = 2;
        private async void button2_Click(object sender, EventArgs e)
        {
            si = loadJson<searchedItems>(Application.StartupPath + "\\search_test.json");
            try
            {
                using (var client = new HttpClient())
                {


                    client.DefaultRequestHeaders.Add("X-Api-Key", "gvqprHwwEIOX8rPBQiLA52cauxkyBOrJIixgWePH");
                    while (page <= si.total_pages)
                    {


                        Uri URI = new Uri("http://swtordata.com:80/api/v2_2/items?page=" + page + "&perpage=50&query=advanced&minimum_level_gte=65");
                        //     var itemName = downloadThis.Text.Split('/');

                        using (var response = await client.GetAsync(URI))
                        using (var stream = new FileStream(Application.StartupPath + "\\search_page" + page + ".json", FileMode.Create, FileAccess.Write, FileShare.None))
                        {
                            await response.Content.CopyToAsync(stream);
                        }

                        //Debug.WriteLine("Download completed");
                        si = loadJson<searchedItems>(Application.StartupPath + "\\search_page" + page + ".json");
                        foreach (var s in si.objects)
                        {
                            comboBox1.Items.Add(s.display_name + "    (Rating: " + s.armor_rating + ")");
                        }

                        Debug.WriteLine("Done with page: " + page);
                        Debug.WriteLine("Items downloaded so far: " + comboBox1.Items.Count + ", current page: " + si.current_page + ", total pages: " + si.total_pages + ", total items: " + si.total_items);
                        using (StreamReader sr = File.OpenText(Application.StartupPath + "\\search_test.json"))
                        {
                            string s = sr.ReadToEnd();
                            richTextBox1.Text = richTextBox1.Text + s;
                        }
                        page++;
                    }


                }
                Debug.WriteLine("Total items:" + comboBox1.Items.Count);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Debug.WriteLine("Could not download data, skipping");

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var tst = comboBox1.Items.Contains(textBox1.Text);
            Debug.WriteLine(tst);

            foreach (var itm in comboBox1.Items)
            {
                if (itm.ToString().Contains(textBox1.Text))
                {
                    Debug.WriteLine(itm.ToString());
                }
            }

        }
        public static Dictionary<String, String> modsDict = new Dictionary<String, String>();
        public static Dictionary<String, searchedItems> itemDict = new Dictionary<String, searchedItems>();
        public static List<List<Object>> stripUneccessaryData(List<searchedItems> data)
        {
            List<List<Object>> filteredData = new List<List<Object>>();
            foreach (var d in data)
            {
                filteredData.Add(d.objects);
            }
            return filteredData;
        }
        public void downloadAndReorganize()
        {
            try
            {
                //infoLabel.Text = "Initilising (written properly), please wait";
                //infoLabel.Visible = true;
                using (var client = new HttpClient())
                {


                    //  Debug.WriteLine("Downloading: " + downloadThis.Text);
                    client.DefaultRequestHeaders.Add("X-Api-Key", "gvqprHwwEIOX8rPBQiLA52cauxkyBOrJIixgWePH");
                    var modtypes = Enum.GetValues(typeof(MODIFICATION));
                    List<searchedItems> itemsCollection = new List<searchedItems>();
                    foreach (var modtype in modtypes)
                    {
                        si = loadJson<searchedItems>(Application.StartupPath + "\\Items\\" + modtype.ToString() + ".json");
                        itemsCollection.Add(si);
                        while (page <= si.total_pages)
                        {
                            si = loadJson<searchedItems>(Application.StartupPath + "\\Items\\" + modtype.ToString() + page + ".json");
                            itemsCollection.Add(si);
                            page++;
                        }
                    }
                    var s = stripUneccessaryData(itemsCollection);
                    Debug.WriteLine("test");
                }
            }


            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Debug.WriteLine("Could not download data, skipping");
            }
            Properties.Settings.Default.firstInit = false;
            Properties.Settings.Default.Save();
            enumarateAndLoadItems();
            infoLabel.Visible = false;

            //     showMainStats();

        }
        public async void getItems()
        {
            try
            {
                infoLabel.Text = "Initilising (written properly), please wait";
                infoLabel.Visible = true;
                using (var client = new HttpClient())
                {


                    //  Debug.WriteLine("Downloading: " + downloadThis.Text);
                    client.DefaultRequestHeaders.Add("X-Api-Key", "gvqprHwwEIOX8rPBQiLA52cauxkyBOrJIixgWePH");
                    var modtypes = Enum.GetValues(typeof(MODIFICATION));
                    foreach (var modtype in modtypes)
                    {


                        Debug.WriteLine(modtype.ToString());

                        Uri URI = new Uri("http://swtordata.com:80/api/v2_2/items?perpage=50&item_slots=Inventory" + "&minimum_level_gte=" + ((modtype.ToString() == "") ? 65 : 70) + "&query=" + ((modtype.ToString() == "Earpiece") ? "Device" : (modtype.ToString() == "Implant") ? "Package" : modtype.ToString()));
                        //     var itemName = downloadThis.Text.Split('/');
                        using (var response = await client.GetAsync(URI))
                        using (var stream = new FileStream(Application.StartupPath + "\\Items\\" + modtype.ToString() + ".json", FileMode.Create, FileAccess.Write, FileShare.None))
                        {
                            await response.Content.CopyToAsync(stream);
                        }

                        Debug.WriteLine("Download completed");
                        si = loadJson<searchedItems>(Application.StartupPath + "\\Items\\" + modtype.ToString() + ".json");
                        Debug.WriteLine("Total Pages:" + si.total_pages);
                        var thiscmb = ((ComboBox)Controls[infoPanel.Name].Controls[modtype.ToString()]);


                        var thislbl = ((Label)Controls[infoPanel.Name].Controls[modtype.ToString() + "Info"]);

                        foreach (var s in si.objects)
                        {
                            thiscmb.Items.Add("Rating: " + s.armor_rating + " | " + s.display_name);
                            thislbl.Text = " Loaded Items:" + thiscmb.Items.Count;
                        }
                        thislbl.Text = "Pages:" + si.total_pages + " Items:" + si.total_items + thislbl.Text;
                        Debug.WriteLine("Done initial load");
                        page = 2;
                        if (si.total_pages > si.current_page)
                        {

                            while (page <= si.total_pages)
                            {
                                //http://swtordata.com/api/v2_2/items?page=2&perpage=50&query=Armoring
                                URI = new Uri("http://swtordata.com:80/api/v2_2/items?page=" + page + "&perpage=50&item_slots=Inventory" + "&minimum_level_gte=" + ((modtype.ToString() == "") ? 65 : 70) + "&query=" + ((modtype.ToString() == "Earpiece") ? "Device" : (modtype.ToString() == "Implant") ? "Package" : modtype.ToString()));

                                using (var response = await client.GetAsync(URI))
                                using (var stream = new FileStream(Application.StartupPath + "\\Items\\" + modtype.ToString() + page + ".json", FileMode.Create, FileAccess.Write, FileShare.None))
                                {
                                    await response.Content.CopyToAsync(stream);
                                }
                                si = loadJson<searchedItems>(Application.StartupPath + "\\Items\\" + modtype.ToString() + page + ".json");
                                foreach (var s in si.objects)
                                {
                                    thiscmb.Items.Add("Rating: " + s.armor_rating + " | " + s.display_name);
                                    thislbl.Text = " Loaded Items:" + thiscmb.Items.Count;
                                }
                                thislbl.Text = "Pages:" + si.total_pages + " Items:" + si.total_items + thislbl.Text;
                                Debug.WriteLine("Done with page: " + page);
                                Debug.WriteLine("Total items with this page:" + thiscmb.Items.Count);
                                page++;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Debug.WriteLine("Could not download data, skipping");
            }
            Properties.Settings.Default.firstInit = false;
            Properties.Settings.Default.Save();
            enumarateAndLoadItems();
            infoLabel.Visible = false;

            //     showMainStats();

        }


        public void enumarateAndLoadItems()
        {

            var modtypes = Enum.GetValues(typeof(MODIFICATION));
            foreach (var modtype in modtypes)
            {


                Debug.WriteLine(modtype.ToString());
                si = loadJson<searchedItems>(Application.StartupPath + "\\Items\\" + modtype.ToString() + ".json");
                Debug.WriteLine("Total Pages:" + si.total_pages);
                var thiscmb = ((ComboBox)Controls[infoPanel.Name].Controls[modtype.ToString()]);


                var thislbl = ((Label)Controls[infoPanel.Name].Controls[modtype.ToString() + "Info"]);

                foreach (var s in si.objects)
                {
                    if (!modsDict.ContainsKey(s.display_name)) modsDict.Add(s.display_name, modtype.ToString());
                    if (!itemDict.ContainsKey(modtype.ToString())) itemDict.Add(modtype.ToString(), si);
                    thiscmb.Items.Add("Rating: " + s.armor_rating + " | " + s.display_name);
                    thislbl.Text = " Loaded Items:" + thiscmb.Items.Count;

                }
                thislbl.Text = "Pages:" + si.total_pages + " Items:" + si.total_items + thislbl.Text;
                Debug.WriteLine("Done initial load");
                page = 2;
                if (si.total_pages > si.current_page)
                {
                    while (page <= si.total_pages)
                    {
                        si = loadJson<searchedItems>(Application.StartupPath + "\\Items\\" + modtype.ToString() + page + ".json");
                        foreach (var s in si.objects)
                        {
                            if (!modsDict.ContainsKey(s.display_name)) modsDict.Add(s.display_name, modtype.ToString() + page);
                            if (!itemDict.ContainsKey(modtype.ToString() + page)) itemDict.Add(modtype.ToString() + page, si);
                            thiscmb.Items.Add("Rating: " + s.armor_rating + " | " + s.display_name);
                            thislbl.Text = " Loaded Items:" + thiscmb.Items.Count;
                        }
                        thislbl.Text = "Pages:" + si.total_pages + " Items:" + si.total_items + thislbl.Text;
                        Debug.WriteLine("Done with page: " + page);
                        Debug.WriteLine("Total items with this page:" + thiscmb.Items.Count);
                        page++;
                    }
                }
            }
            showMainStats();
        }



        public void makePanel(String gear, int i = 0)
        {

            Panel p = new Panel();
            i = (i <= 0) ? i = 0 : i;
            var namestr = gear + ((i == 0) ? "" : i.ToString());
            p.Name = namestr + "Panel";
            p.BorderStyle = BorderStyle.FixedSingle;
            p.Size = fourPanel.Size;
            var lbl = new Label();
            lbl.Name = namestr;
            lbl.Text = namestr;

            p.Controls.Add(lbl);
            //    lbl.Location = new Point(0, 0);
            switch (gear)
            {
                case "Relic1":
                case "Relic2":
                    {
                        ComboBox cb = new ComboBox();
                        cb.Items.AddRange(Relic.Items.Cast<object>().ToArray());
                        p.Controls.Add(cb);
                        cb.Name = gear;
                        ComboBox cb1 = new ComboBox();
                        cb1.Items.AddRange(Augment.Items.Cast<object>().ToArray());
                        cb1.Name = gear + "Augment";
                        p.Controls.Add(cb1);
                        cb.Top = lbl.Bottom;
                        cb1.Top = cb.Bottom;
                        break;
                    }
                case "Implant1":
                case "Implant2":
                    {
                        ComboBox cb = new ComboBox();
                        cb.Items.AddRange(Implant.Items.Cast<object>().ToArray());
                        p.Controls.Add(cb);
                        cb.Name = gear;
                        ComboBox cb1 = new ComboBox();
                        cb1.Items.AddRange(Augment.Items.Cast<object>().ToArray());
                        cb1.Name = gear + "Augment";
                        p.Controls.Add(cb1);
                        cb.Top = lbl.Bottom;
                        cb1.Top = cb.Bottom;
                        break;
                    }
                case "Earpiece":
                    {
                        ComboBox cb = new ComboBox();
                        cb.Items.AddRange(Earpiece.Items.Cast<object>().ToArray());
                        p.Controls.Add(cb);
                        cb.Name = gear;
                        ComboBox cb1 = new ComboBox();
                        cb1.Items.AddRange(Augment.Items.Cast<object>().ToArray());
                        cb1.Name = gear + "Augment";
                        p.Controls.Add(cb1);
                        cb.Top = lbl.Bottom;
                        cb1.Top = cb.Bottom;
                        break;
                    }
                case "Bracer":
                case "Belt":
                    {
                        ComboBox cb = new ComboBox();
                        cb.Items.AddRange(Armoring.Items.Cast<object>().ToArray());
                        p.Controls.Add(cb);
                        cb.Name = gear + "Armoring";
                        ComboBox cb1 = new ComboBox();
                        cb1.Items.AddRange(Mod.Items.Cast<object>().ToArray());
                        p.Controls.Add(cb1);
                        cb1.Name = gear + "Mod";
                        ComboBox cb2 = new ComboBox();
                        cb2.Items.AddRange(Augment.Items.Cast<object>().ToArray());
                        cb2.Name = gear + "Augment";
                        p.Controls.Add(cb2);
                        cb.Top = lbl.Bottom;
                        cb1.Top = cb.Bottom;
                        cb2.Top = cb1.Bottom;
                        break;
                    }
                default:
                    {
                        ComboBox cb = new ComboBox();
                        cb.Name = gear + "Armoring";
                        cb.Items.AddRange(Armoring.Items.Cast<object>().ToArray());
                        p.Controls.Add(cb);
                        ComboBox cb1 = new ComboBox();
                        cb1.Name = gear + "Mod";
                        cb1.Items.AddRange(Mod.Items.Cast<object>().ToArray());
                        p.Controls.Add(cb1);
                        ComboBox cb2 = new ComboBox();
                        cb2.Items.AddRange(Enhancement.Items.Cast<object>().ToArray());
                        p.Controls.Add(cb2);
                        ComboBox cb3 = new ComboBox();
                        cb3.Name = gear + "Augment";
                        cb3.Items.AddRange(Augment.Items.Cast<object>().ToArray());
                        p.Controls.Add(cb3);
                        cb.Top = lbl.Bottom;
                        cb1.Top = cb.Bottom;
                        cb3.Top = cb1.Bottom;
                        break;
                    }

            }
            charPanel.Controls.Add(p);
            p.BackColor = Color.Transparent;
            p.Size = new Size(charPanel.Width / 2, p.Height);
            foreach (var ctrl in p.Controls)
            {
                ((Control)ctrl).Width = p.Width;
                if (ctrl.GetType() == typeof(ComboBox))
                {
                    ((ComboBox)ctrl).Sorted = true;
                    ((ComboBox)ctrl).SelectedIndexChanged += new System.EventHandler(customComboBox);
                    ((ComboBox)ctrl).DrawMode = DrawMode.OwnerDrawFixed;
                    ((ComboBox)ctrl).DrawItem += cb_DrawItem;
                    ((ComboBox)ctrl).DropDownClosed += cb_DropDownClosed;
                }
            }

            if (charPanel.Controls.Count == 1)
            {
                p.Location = new Point(0, 0);
            }
            else if (charPanel.Controls.Count == 8)
            {
                var prevCtrl = charPanel.Controls[0];
                p.Location = new Point(charPanel.Width - p.Width, 0);
            }
            else
            {
                var prevCtrl = charPanel.Controls[charPanel.Controls.IndexOf(p) - 1];
                p.Location = new Point(prevCtrl.Left, prevCtrl.Bottom);
            }
            charPanel.Height = charPanel.Controls[charPanel.Controls.Count - 1].Bottom;
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == WM_NCHITTEST)
                m.Result = (IntPtr)(HT_CAPTION);
        }

        private const int WM_NCHITTEST = 0x84;
        private const int HT_CLIENT = 0x1;
        private const int HT_CAPTION = 0x2;
        Form2 f2 = new Form2();


        public static void testCode()
        {
            Parser p = new Parser(new StreamReader(new FileStream(Application.StartupPath + @"\Items\Relic.json", FileMode.Open)));
            List<JsonItem> items = p.root.Objects.Where(x => x.Value != 0)
                .Select(x => x as JsonItem).OrderBy(x => x.DisplayName)
                .ToList();
            items.ForEach(x => Console.WriteLine(string.Format("{0} - {1} - {2} - Bind {3} - {4} Quality", x.ID, x.DisplayName, x.ArmourRating, x.Binding, x.Quality.Name)));

         //   Console.ReadLine();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //ova e za prvata slika, edituvaj gi sliktie za da bidat isti

            this.BackgroundImageLayout = ImageLayout.Stretch;
            this.BackgroundImage = Properties.Resources.datapad_v101;
            this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width), (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2 - 100);

           // Directory.CreateDirectory(Application.StartupPath + "\\Items\\");
            testCode();

            //   Properties.Settings.Default.firstInit = true;
          //  downloadAndReorganize();
            
            //if (Properties.Settings.Default.firstInit) getItems();
            //else enumarateAndLoadItems();



        }


        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            //ova e za formata
            this.Size = new Size(600, 800);
            Rectangle Bounds = new Rectangle(0, 0, this.Width, this.Height);
            int CornerRadius = 30;
            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
            path.AddArc(Bounds.X, Bounds.Y, CornerRadius, CornerRadius, 180, 90);
            path.AddArc(Bounds.X + Bounds.Width - CornerRadius, Bounds.Y, CornerRadius, CornerRadius, 270, 90);
            path.AddArc(Bounds.X + Bounds.Width - CornerRadius, Bounds.Y + Bounds.Height - CornerRadius, CornerRadius, CornerRadius, 0, 90);
            path.AddArc(Bounds.X, Bounds.Y + Bounds.Height - CornerRadius, CornerRadius, CornerRadius, 90, 90);
            path.CloseAllFigures();

            this.Region = new Region(path);
            //  this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
        }
        public enum GEAR
        {

            Earpiece,
            Implant1,
            Implant2,
            Bracers,
            Relic1,
            Relic2,
            MH,
            Head,
            Chest,
            Gloves,
            Belt,
            Pants,
            Boots,
            OH
        }

        public enum MODIFICATION
        {

            Armoring,
            Mod,
            Enhancement,
            Augment,
            Relic,
            Earpiece,
            Implant,
            //Crystal // not implemented yet
        }
        public void showMainStats()
        {

            //tuka se pojavuvaat panelite
            var gearTypes = Enum.GetValues(typeof(GEAR));
            foreach (var gear in gearTypes)
            {
                makePanel(gear.ToString());

                //switch (gear.ToString())
                //{
                //    case "Relic":
                //    case "Implant":
                //        {
                //            makePanel(gear.ToString(), 1);
                //            break;
                //        }
                //}
                Debug.WriteLine("Panel " + gear.ToString() + " created");
            }
            charPanel.Visible = true;
            //charPanel.Size = new Size(500, 700);
            charPanel.Location = new Point(30, 50);
            this.BackgroundImage = Properties.Resources.datapad600x845v2;

        }

        public Object findTheMod(searchedItems si, String searchParam)
        {
            foreach (var s in si.objects)
            {
                if (s.display_name.Equals(searchParam))
                {
                    return s;
                }
            }
            return null;
        }
        public StatBlock loadStatsForSelection(String selectedItem)
        {
            selectedItem = selectedItem.Split('|')[1].Trim();
            Object searchedItem = null;
            try
            {
                searchedItem = findTheMod(itemDict[modsDict[selectedItem]], selectedItem);
            }
            catch (Exception ex)
            {
                return null;
            }
            StatBlock sb = new StatBlock();
            foreach (var sss in searchedItem.base_stats)
            {


                sb.name = searchedItem.display_name;
                switch (sss.item_stat.item_stat_type.display_text)
                {
                    case ("Mastery"): { sb.Mastery = sss.item_stat.stat_amount; break; }
                    case ("Endurance"): { sb.Endurance = sss.item_stat.stat_amount; break; }
                    case ("Power"): { sb.Power = sss.item_stat.stat_amount; break; }
                    case ("Critical Rating"): { sb.Critical = sss.item_stat.stat_amount; break; }
                    case ("Alacrity Rating"): { sb.Alacrity = sss.item_stat.stat_amount; break; }
                    case ("Accuracy Rating"): { sb.Accuracy = sss.item_stat.stat_amount; break; }
                    case ("Defense Rating"): { sb.Defence = sss.item_stat.stat_amount; break; }
                    case ("Absorption Rating"): { sb.Absorb = sss.item_stat.stat_amount; break; }
                    case ("Shield Rating"): { sb.Shield = sss.item_stat.stat_amount; break; }
                }


                //  if ( sss.item_stat.item_stat_type.display_text.Equals("Endurance")) EnduranceLabel.Text = ((int.Parse(EnduranceLabel.Text)) + sss.item_stat.stat_amount).ToString();
            }
            // Debug.WriteLine(searchedItem.display_name);
            return sb;
        }
        private void Form1_Shown(object sender, EventArgs e)
        {


        }

        private void button4_Click(object sender, EventArgs e)
        {

            var gearTypes = Enum.GetValues(typeof(GEAR));
            FullGear fg = new FullGear();
            foreach (var gear in gearTypes)
            {

                GearPiece gp = new GearPiece();
                gp.type = (GEAR)gear;
                foreach (var ctrl in Controls["charPanel"].Controls[gear.ToString() + "Panel"].Controls)
                {
                    if (ctrl.GetType() == typeof(ComboBox) && ((ComboBox)ctrl).SelectedIndex >= 0)
                    {

                        if (((ComboBox)ctrl).Name.Contains("Augment"))
                        {
                            //gp.GetType().GetProperty("augment").SetValue(gp, loadStatsForSelection(((ComboBox)ctrl).SelectedItem.ToString()));
                            gp.augment = loadStatsForSelection(((ComboBox)ctrl).SelectedItem.ToString());
                        }
                        else if (((ComboBox)ctrl).Name.Contains("Mod"))
                        {
                            gp.mod = loadStatsForSelection(((ComboBox)ctrl).SelectedItem.ToString());
                        }
                        else if (((ComboBox)ctrl).Name.Contains("Armoring"))
                        {

                            gp.armoring = loadStatsForSelection(((ComboBox)ctrl).SelectedItem.ToString());
                        }
                        else
                        {
                            gp.misc = loadStatsForSelection(((ComboBox)ctrl).SelectedItem.ToString());
                        }
                    }
                }
                if (!(gp == null)) fg.fullGear.Add(gp);
            }
            //   MessageBox.Show(gp.GearPieceInfo());

            //f2.Opacity = 0;

            //f2.ShowInTaskbar = false;
            //f2.ShowDialog();
            //   Location = new Point(Location.X - 50, Location.Y);
            if (f2.active == 0)
            {
                Location = new Point(Location.X - 100, Location.Y);
                f2.active = 1;

            }

            f2.BackgroundImage = BackgroundImage;
            f2.BackgroundImageLayout = BackgroundImageLayout;
            f2.StartPosition = FormStartPosition.Manual;
            f2.Controls["RichTextBox2"].Text = "";
            f2.Controls["leftSide"].Text = fg.displayFullGearInfo(1);
            f2.Controls["rightSide"].Text = fg.displayFullGearInfo(2);
            Dictionary<string, int> count = new Dictionary<string, int>();
            foreach (var ggg in fg.fullGear)
            {
                if (ggg.enhancement.isNull())
                {
                    if (!count.ContainsKey(ggg.enhancement.name))
                    {
                        count.Add(ggg.enhancement.name, 1);
                    }
                    else count[ggg.enhancement.name]++;
                }
                if (ggg.armoring.isNull())
                {
                    if (!count.ContainsKey(ggg.armoring.name))
                    {
                        count.Add(ggg.armoring.name, 1);
                    }
                    else count[ggg.armoring.name]++;
                }
                if (ggg.augment.isNull())
                {
                    if (!count.ContainsKey(ggg.augment.name))
                    {
                        count.Add(ggg.augment.name, 1);
                    }
                    else count[ggg.augment.name]++;
                }
                if (ggg.mod.isNull())
                {
                    if (!count.ContainsKey(ggg.mod.name))
                    {
                        count.Add(ggg.mod.name, 1);
                    }
                    else count[ggg.mod.name]++;
                }
                if (ggg.misc.isNull())
                {
                    if (!count.ContainsKey(ggg.misc.name))
                    {
                        count.Add(ggg.misc.name, 1);
                    }

                    else count[ggg.misc.name]++;
                }
            }

            foreach (var v in count.Keys)
            {
                f2.Controls["RichTextBox2"].Text = f2.Controls["RichTextBox2"].Text + "\n" + count[v] + " x " + v;
            }

            f2.Show();
            // f2.Location = new Point((this.Location.X + Width), (this.Location.Y + Height - f2.Height) / 2);
        }


        private void cb_DropDownClosed(object sender, EventArgs e)
        {
            toolTip1.Hide(comboBox1);
        }

        private void cb_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index > 0)
            { // added this line thanks to Andrew's comment
                string text = comboBox1.GetItemText(((ComboBox)sender).Items[e.Index]);
                var str = loadStatsForSelection(((ComboBox)sender).Items[e.Index].ToString()).StatBlockToString();
                e.DrawBackground();
                using (SolidBrush br = new SolidBrush(e.ForeColor))
                { e.Graphics.DrawString(text, e.Font, br, e.Bounds); }
                var cpos = PointToClient(Cursor.Position);
                Debug.WriteLine("CLIENT POINT: " + cpos);
                Debug.WriteLine("CURSOR POINT: " + Cursor.Position);
                if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                { toolTip1.Show(str, this, cpos.X, cpos.Y); }
                // Debug.WriteLine(e.Bounds);
                e.DrawFocusRectangle();
            }
        }

        private void customComboBox(object sender, EventArgs e)
        {
            //     loadStatsForSelection(((ComboBox)sender).SelectedItem.ToString()).StatBlockToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_LocationChanged(object sender, EventArgs e)
        {
            //if (f2.active == 0)
            //{
            f2.Location = new Point((this.Location.X + Width), (this.Location.Y + Height - f2.Height) / 2);
            //}
        }

    }
}
