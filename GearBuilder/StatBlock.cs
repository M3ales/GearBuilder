﻿

using System;

namespace GearBuilder
{
    public class StatBlock
    {

        public int Mastery;
        public int Endurance;
        public int Critical;
        public int Power;
        public int Alacrity;
        public int Accuracy;
        public int Defence;
        public int Absorb;
        public int Shield;
        public string name;



        public string StatBlockToString()
        {
            String sb = null;
            sb = sb + ((Mastery != 0) ? "Mastery\t\t" + Mastery + "\n" : "");
            sb = sb + ((Endurance != 0) ? "Endurance\t" + Endurance + "\n" : "");
            sb = sb + ((Critical != 0) ? "Critical\t\t" + Critical + "\n" : "");
            sb = sb + ((Power != 0) ? "Power\t\t" + Power + "\n" : "");
            sb = sb + ((Alacrity != 0) ? "Alacrity\t\t" + Alacrity + "\n" : "");
            sb = sb + ((Accuracy != 0) ? "Accuracy\t" + Accuracy + "\n" : "");
            sb = sb + ((Defence != 0) ? "Defence\t\t" + Defence + "\n" : "");
            sb = sb + ((Absorb != 0) ? "Absorb\t\t" + Absorb + "\n" : "");
            sb = sb + ((Shield != 0) ? "Shield\t\t" + Shield + "\n" : "");
            return sb;
        }
        public bool isNull()
        {
            if (0 == Mastery && 0 == Endurance
                && 0 == Critical && 0 == Power
                && 0 == Alacrity && 0 == Accuracy
                && 0 == Defence && 0 == Absorb
                && 0 == Shield && String.IsNullOrEmpty(name))
                return false;

            return true;
        }
    }



}
